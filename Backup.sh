                                                                                                   
#i/bin/bash                                                                                        
# Display welcome message, computer name and date                                                  
echo "*** Backup shell script ***"                                                                 
echo                                                                                               
echo "*** Run time; $(date) @ $(hostname)"                                                         
echo                                                                                               
# Define variable                                                                                  
BACKUP="/home/augie"                                                                               
NOW=$(date +"%d-%m-%Y")                                                                            
# Let us start backup                                                                              
echo "*** Dumping Direktory to $BACKUP/$NOW..."                                                    
mkdir $BACKUP/$NOW                                                                                 
cd $BACKUP/$NOW                                                                                    
tar -czvf latest.tar.gz $BACKUP                                                                    
                                                                                                   
# Just sleep for 3 secs                                                                            
sleep 3                                                                                            
# And we are done...                                                                               
echo                                                                                               
echo "*** Backup wrote to $BACKUP/$NOW/latest.tar.gz"