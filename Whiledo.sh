#!/bin/bash                                                          
# set an infinite loop                                               
while :                                                              
do                                                                   
clear                                                                
# display menu                                                       
echo "Server Name - $(hostname)"                                     
echo "______________________________"                                
echo "      M A I N - M E N U"                                       
echo "______________________________"                                
echo "A. Aritmatika"                                                 
echo "B. Tentang Pembuat."                                           
echo "C. Exit"                                                       
# get input from the user                                            
read -p "Enter your choice [ A - C ] " choice                        
# make decisiom using case..in..esac                                 
case $choice in                                                      
        A)                                                           
                #set an infinite loop                                
                clear                                                
                                                                     
                #display menu                                        
                echo "------------------------------------------"    
                echo "        A R I T M A T I K A       "    
                echo "------------------------------------------"    
                echo "1. Penjumlahan"                                
                echo "2. Pengurangan"                                
                echo "3. Perkalian"                                  
                echo "4. Pembagian"                                  
                echo "5. Back to Menu"                               
                                                                     
                #get input from the user                             
                read -p "[Enter your choice [1-5]" pilihanA          
                                                                     
                #make decision using case..in..esac                  
                case $pilihanA in                                    
        1)                                                           
                echo "----- PENJUMLAHAN -----"                       
                echo "Masukan Angka Pertama"                         
                read var1a                                           
                echo "Masukan angka kedua"                           
                read var2a                                           
                let a=var1a+var2a                                    
                echo "Hasil dari $var1a + $var2a = $a"               
                  read -p "Press [Enter] key to continue..."         
                  readEnterKey
                  ;;                                                 
        2)      echo "----- PENGURANGAN -----"                       
                echo "Masukan angka pertama"                         
                read var1b                                           
                echo "Masukan angka kedua"                           
                read var2b                                           
                let b=var1b-var2b                                    
                echo "Hasil dari $var1b - $var2b = $b"               
                  read -p "Press [Enter] key to continue..."         
                  readEnterKey                                       
                  ;;                                                 
        3)                                                           
                echo "----- PERKALIAN -----"                         
                echo "Masukan angka pertama"                         
                read var1c                                           
                echo "Masukan angka kedua"                           
                read var2c                                           
                let c=var1c*var2c                                    
                echo "Hasil dari $var1c x $var2c = $c"               
                  read -p "Press [Enter] key to continue..."         
                  readEnterKey                                       
                  ;;                                                 
        4)                                                           
                echo "----- PEMBAGIAN -----"
                echo "Masukan angka pertama"                         
                read var1d                                           
                echo "Masukan angka kedua"                           
                read var2d                                           
                let d=var1d/var2d                                    
                let e=var1d%var2d                                    
                echo "Hasil dari $var1d : $var2d = $d sisa $e"       
                read -p "Press [Enter] key to continue..."           
                readEnterKey                                         
                  ;;                                                 
        5)                                                           
                bash whiledo                                         
                ;;                                                   
                *)                                                   
        echo "Error: Invalid option..."                              
        read -p "Press [Enter] key to continue..."                   
        readEnterKey                                                 
        ;;                                                           
esac                                                                 
;;
B)                                                                   
        #set an infinite loop                                        
        clear                                                        
                                                                     
        #display menu                                                
        echo "----------------------------------"                    
        echo "    P E M B U A T    "                    
        echo "----------------------------------"                    
        echo "1. Nama, NIM, Home Dir"                                
        echo "2. Username@ Hostname"                                 
        echo "3. Back To Menu"
                                                                     
        #get input from the user                                     
        read -p "[Enter your choice [1-3]" pilihanB                  
                                                                     
        #make decision using case..in..esac                          
        case $pilihanB in                                            
        1)                                                           
                echo "Nama : Augie Mahardika Devianto"                    
                echo "NIM : 2197200875"                              
                echo "Home Dir : /home/augie"                         
                read -p "Press [Enter] key to continue..."           
                readEnterKey                                         
                ;;                                                   
        2)                                                           
                echo "augie@$(hostname)"                              
                read -p "Press [Enter] Key to continue.."            
                readEnterKey                                         
                ;;                                                   
        3)                                                           
                bash whiledo                                         
                ;;                                                   
                *)                                                   
        echo "Error: Invalid option..."
        read -p "Press [Enter] key to continue..."                   
        readEnterKey                                                 
        ;;                                                           
        esac                                                         
        ;;                                                           
                                                                     
        C)                                                           
                clear                                                
                exit 0                                               
                ;;                                                   
                *)                                                   
        echo "Error: Invalid option..."                              
        read -p "Press [Enter] key to continue..."                   
        readEnterKey                                                 
        ;;                                                           
esac                                                                 
done                                                                 
;;